<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_text_attributes" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_web"/>
    <link type="seealso" xref="howto_text_attributes"/>
    <title type="sort">8. Atributos de Texto</title>
    <title type="link">Atributos de texto</title>
    <desc>Configurando qual formatação é apresentada</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Preferências de Atributos de Texto</title>
  <p>O termo "atributos de texto" se refere à fonte, estilo, alinhamento, e outras formatações associadas a um determinado caractere ou série de caracteres. A página de <gui>atributos de texto</gui> do <app>Orca</app> permite que você personalize quais atributos de texto o <app>Orca</app> apresentará na fala, juntamente com a ordem na qual eles devem ser apresentados, e quais o <app>Orca</app> indicará em Braille.</p>
  <section id="table">
    <title>A tabela de atributos de texto</title>
    <p>A tabela de atributos de texto é onde você especifica quais atributos serão ou não apresentados, e em que condições. Cada linha consiste de quatro colunas.</p>
    <list>
      <item>
        <p><gui>Nome do Atributo</gui>: O nome do atributo de texto.</p>
      </item>
      <item>
        <p><gui>Fala</gui>: Marque esta caixa de seleção se desejar que o <app>Orca</app> fale este atributo quando você pressionar <keyseq><key>Tecla Modificadora do Orca</key><key>F</key></keyseq>.</p>
      </item>
      <item>
        <p><gui>Marcar em braile</gui>: Marque esta caixa de seleção se você deseja que o <app>Orca</app> "sublinhe" este atributo em sua linha braile.</p>
      </item>
      <item>
        <p><gui>Presente a menos que</gui>: Este campo editável permite que você especifique quando um atributo habilitado não é de interesse.</p>
        <p>Por exemplo, por padrão, o atributo de texto "sublinhado" tem o valor "nenhum". Isso faz com que o <app>Orca</app> informe você sobre o texto sublinhado, desde que o texto esteja realmente sublinhado. Se você sempre quiser que este atributo seja falado independentemente de o texto estar ou não sublinhado, a coluna <gui>Apresentar a menos que</gui> deve estar vazia para sublinhado. Além disso, você deve ter certeza de que a coluna <gui>Falar</gui> para sublinhado está marcada.</p>
      </item>
    </list>
  </section>
  <section id="reset">
     <title>Desfazendo alterações</title>
     <p>Abaixo da lista de atributos de texto, há um botão <gui>Redefinir</gui> (<keyseq><key>Alt</key><key>R</key></keyseq>) que irá restaurar os valores da tabela para o que eles eram quando a caixa de diálogo foi exibida pela primeira vez.</p>
  </section>
  <section id="rearranging">
     <title>Reorganizando a ordem de apresentação</title>
     <p>Quando você exibe inicialmente a página <gui>Atributos do Texto</gui>, todos os seus atributos habilitados são colocados no topo da tabela na ordem em que serão falados. Existem quatro botões que podem ser usados para reorganizar a ordem de apresentação.</p>
    <list>
      <item>
        <p><gui>Move para o início</gui> (<keyseq><key>Alt</key><key>T</key></keyseq>): move o atributo selecionado para o início da lista.</p>
      </item>
      <item>
        <p><gui>Move para cima</gui> (<keyseq><key>Alt</key><key>U</key></keyseq>): move o atributo selecionado uma linha acima.</p>
      </item>
      <item>
        <p><gui>Mover para baixo</gui> (<keyseq><key>Alt</key><key>D</key></keyseq>): move o atributo selecionado uma linha abaixo.</p>
      </item>
      <item>
        <p><gui>Move para o fim</gui> (<keyseq><key>Alt</key><key>B</key></keyseq>): move o atributo selecionado para o final da lista.</p>
      </item>
    </list>
  </section>
  <section id="braille_indicator">
    <title>Opções para configurar a formatação do "Sublinhado" em Braille</title>
    <p>Abaixo dos botões está o grupo de botões de rádio <gui>Indicador Braille</gui>. Aqui você pode selecionar a célula ou células a serem usadas para indicar o texto que possui pelo menos um dos atributos especificados.</p>
    <list>
      <item>
        <p><gui>Nenhum</gui>: Não sublinha atributos do texto em braile (padrão)</p>
      </item>
      <item>
        <p><gui>Ponto 7</gui>: Sublinha atributos do texto apenas com o Ponto 7</p>
      </item>
      <item>
        <p><gui>Ponto 8</gui>: Sublinha atributos do texto apenas com o Ponto 8</p>
      </item>
      <item>
        <p><gui>Pontos 7 e 8</gui>: Sublinha atributos do texto com os Pontos 7 e 8</p>
      </item>
    </list>
  </section>
</page>
