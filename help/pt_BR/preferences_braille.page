<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_braille" xml:lang="pt-BR">
  <info>
    <title type="sort">3. Braille</title>
    <title type="link">Braille</title>
    <desc>Configurando o suporte à linha braile do <app>Orca</app></desc>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_key_echo"/>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Preferências Braille</title>
  <section id="braillesupport">
    <title>Habilitar Suporte ao Braille</title>
    <p>Esta caixa de seleção informa se o <app>Orca</app> fará uso de um dispositivo Braille. Se o BrlTTY não estiver em execução, o <app>Orca</app> recuperar-se-á de forma elegante sem comunicar a falha para o dispositivo Braille.</p>
    <p>Valor padrão: não selecionado</p>
    <note style="tip">
      <p>Se você configurar o BrlTTY posteriormente, você deve reiniciar o <app>Orca</app> para usar o suporte a Braille.</p>
    </note>
  </section>
  <section id="braillewordwrap">
    <title>Habilitar quebra de linha</title>
    <p>Se <gui>Ativar quebra de linha</gui> estiver marcado, o <app>Orca</app> ajustará o texto para que apenas palavras completas sejam mostradas na linha braille. Se não estiver marcado, o <app>Orca</app> usará todas as células na tela para que mais texto possa ser mostrado de uma só vez.</p>
    <p>Valor padrão: não selecionado</p>
  </section>
  <section id="contractedbraille">
    <title>Habilitar Braille Abreviado</title>
    <p>O suporte do Orca a Braille contraído deve-se ao projeto Liblouis. Visto que muitas distribuições Linux incluem Liblouis, você provavelmente terá acesso automático ao suporte ao Braille contraído no <app>Orca</app>.</p>
    <p>Para habilitar braille abreviado em um sistema onde o liblouis foi instalado, certifique-se de que a caixa de seleção <gui>Habilitar braille abreviado</gui> esteja marcada. Em seguida, escolha a tabela de tradução desejada na caixa de combinação <gui>Tabela de contração</gui>.</p>
    <p>Valor padrão: não selecionado</p>
  </section>
  <section id="rolenames">
    <title>Nomes abreviados de funções</title>
    <p>Esta caixa de seleção determina de que maneira os nomes de regras são exibidos e como podem ser utilizados para conservar o estado real no dispositivo Braille. Por exemplo, se um controle deslizante ganha foco, a palavra "deslizante (ou slider)" é exibida se os nomes de regras abreviados não tiverem sido marcados; se, ao contrário, tiverem sido marcados, "dslz", é exibido.</p>
    <p>Valor padrão: não selecionado</p>
  </section>
  <section id="eolindicator">
    <title>Desabilitar o símbolo de fim de linha</title>
    <p>Marcar esta caixa de seleção diz ao <app>Orca</app> para não apresentar a string "$l" no final de uma linha de texto.</p>
    <p>Valor padrão: não selecionado</p>
  </section>
  <section id="verbosity">
    <title>Verbosidade</title>
    <p>Este grupo de botões de opção determina a quantidade de informação que será exibida pelo dispositivo Braille em certas situações. Por exemplo, se a opção verbosa está selecionada, informações sobre atalhos de teclado e nomes de regras são exibidas. Estas informações não são exibidas no modo resumido.</p>
    <p>Valor padrão: <gui>Detalhado</gui></p>
  </section>
  <section id="selectionandhyperlink">
    <title>Indicadores de seleção e de links</title>
    <p>O grupo de botões de opção para <gui>indicador de seleção</gui> e <gui>indicador de link</gui> permite-lhe configurar o comportamento do <app>Orca</app> quando exibe textos selecionados e links. Por padrão, quando você encontra um deles, o <app>Orca</app> irá sublinhar o texto em sua linha Braille com os pontos 7 e 8. Se preferir, você pode alterar o indicador para ser apenas o ponto 7, ser apenas o ponto 8, ou não estar presente.</p>
    <p>Valor padrão: <gui>7 e 8 pontos</gui></p>
    <note style="tip">
      <title>Indicadores de atributo de texto</title>
      <p>Opcionalmente, você pode ter os atributos de texto indicados em sua linha Braille. Habilitando este recurso e escolhendo quais são os atributos que lhe interessam, o que é feito na <link xref="preferences_text_attributes">página <gui>atributos de textos</gui></link> do diálogo de Preferências.</p>
    </note>
  </section>
  <section id="flashMessageSettings">
    <title>Configurações de Mensagem Flash</title>
    <p>As mensagens flash são de natureza semelhante a notificações ou anúncios: elas são exibidas em sua linha braile atualizável por um breve período, após o qual o conteúdo original é restaurado. O <app>Orca</app> tem várias configurações que você pode usar para controlar a apresentação de mensagens flash.</p>
    <section id="flashMessageSettings_enable">
      <title>Ativar mensagens flash</title>
      <p>Se <gui>Ativar mensagens flash</gui> estiver marcado, o <app>Orca</app> apresentará mensagens para você em braille. Se você preferir que apenas as mensagens do <app>Orca</app> sejam faladas, desmarque esta caixa de seleção.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="flashMessageSettings_detailed">
      <title>Mensagens detalhadas</title>
      <p>Se <gui>Mensagens detalhadas</gui> estiver marcada, o <app>Orca</app> apresentará mensagens detalhadas para você em braille. Por exemplo, se você usar o comando do <app>Orca</app> para alterar o eco, o <app>Orca</app> poderá exibir "Eco definido como palavra". Se você preferir mensagens mais curtas, como simplesmente "palavra", desmarque essa caixa de seleção.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="flashMessageSettings_persistent">
      <title>Mensagens persistentes</title>
      <p>Como dito acima, as mensagens flash são exibidas apenas por um breve período de tempo. Se você preferir que as mensagens permaneçam exibidas até que você execute uma ação que faça com que sua exibição seja atualizada, você deve marcar a caixa de seleção <gui>As mensagens são persistentes</gui>.</p>
      <p>Valor padrão: não selecionado</p>
    </section>
    <section id="flashMessageSettings_duration">
      <title>Duração (segundos)</title>
      <p>A quantidade de tempo que o <app>Orca</app> aguardará antes de remover a mensagem e restaurar o conteúdo original de sua exibição pode ser definida no botão giratório <gui> Duração (segs)</gui>. Observe que o valor dessa configuração será ignorado se você tiver ativado mensagens flash persistentes.</p>
      <p>Valor padrão: 5</p>
    </section>
  </section>
</page>
