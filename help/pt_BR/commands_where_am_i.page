<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="commands_where_am_i" xml:lang="pt-BR">
  <info>
    <link type="next" xref="commands_time_date_notifications"/>
    <link type="guide" xref="commands#getting_started"/>
    <link type="seealso" xref="howto_whereami"/>
    <title type="sort">3. Onde Estou</title>
    <title type="link">Onde estou</title>
    <desc>Comandos para saber sobre sua localização</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Comandos para onde estou</title>
  <p>O recurso Onde Estou do <app>Orca</app> fornece detalhes contextuais sobre sua localização atual. Por exemplo, em tabelas, o recurso fornecerá detalhes sobre a célula da tabela em que você está. Em texto, apresentará a linha atual junto com qualquer texto selecionado. A lista completa do que você pode esperar que o <app>Orca</app> apresente pode ser encontrada na <link xref="howto_whereami">Introdução do Onde Estou</link>.</p>
  <p>O <app>Orca</app> fornece os seguintes comandos Onde Estou:</p>
  <list>
    <item>
      <p>Executa o comando Onde Estou básico:</p>
      <list>
        <item>
          <p>Desktop: <key>NUM Enter</key></p>
        </item>
        <item>
          <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Enter</key></keyseq></p>
        </item>
      </list>
    </item>
    <item>
      <p>Executa o comando Onde Estou detalhado:</p>
      <list>
        <item>
          <p>Desktop: <key>NUM Enter</key> (clique duas vezes)</p>
        </item>
        <item>
          <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Enter</key></keyseq> (clique duas vezes)</p>
        </item>
      </list>
    </item>
  </list>
  <p>Além dos comandos dedicados Onde Estou, o <app>Orca</app> possui comandos adicionais relacionados à obtenção de informações sobre sua localização atual:</p>
  <list>
    <item>
      <p>Apresenta a barra de título:</p>
      <list>
        <item>
          <p>Desktop: <keyseq><key>Tecla Modificadora do Orca</key><key>NUM Enter</key></keyseq></p>
        </item>
        <item>
          <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Barra</key></keyseq></p>
        </item>
      </list>
    </item>
    <item>
      <p>Apresenta a barra de estado:</p>
      <list>
        <item>
          <p>Desktop: <keyseq><key>Tecla Modificadora do Orca</key><key>NUM Enter</key></keyseq> (clique duas vezes)</p>
        </item>
        <item>
          <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Barra</key></keyseq> (clique duas vezes)</p>
        </item>
        <item>
          <p>Apresenta tamanho e localização do objeto atual em pixels: (Sem atalho definido)</p>
        </item>
      </list>
    </item>
  </list>
</page>
