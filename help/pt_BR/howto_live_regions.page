<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_live_regions" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reading"/>
    <link type="next" xref="howto_whereami"/>
    <title type="sort">6. Regiões Dinâmicas</title>
    <desc>Interagindo com conteúdo dinâmico da web</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Regiões dinâmicas</title>
  <p>Uma região dinâmica é uma parte atualizada dinamicamente de uma página da Web, como uma tabela de estatísticas esportivas, uma lista de preços de ações atuais, um registro de um chat ou um alerta exibido pela página que você está lendo. Embora as regiões dinâmicas apareçam com bastante frequência, as páginas da Web totalmente acessíveis com esses elementos são encontradas com menos frequência. Este problema está sendo abordado ativamente por várias organizações.</p>
  <section id="politeness_levels">
    <title>Nível de polidez de uma região dinâmica</title>
    <p>As regiões dinâmicas têm um nível de "cortesia" associado que é definido pelo autor como um meio de transmitir a importância da informação e sugerir quando os usuários devem ser informados por sua tecnologia assistiva das atualizações feitas naquela região. As regiões dinâmicas podem ser "desativadas", "educadas" ou "assertivas" a ponto de serem "rude".</p>
  </section>
  <section id="orca_support">
    <title>Suporte do <app>Orca</app> para Regiões Dinâmicas</title>
    <p>Como você pode não concordar com o nível de cortesia especificado pelo autor cuja página você está visualizando, o <app>Orca</app> fornece vários <link xref="commands_live_regions">comandos da região dinâmica</link> que permitem que você modiique o nível de uma ou de todas as regiões em uma página. Além disso, você pode:</p>
    <list>
      <item>
        <p>Ligando ou desligando o suporte às regiões dinâmicas</p>
      </item>
      <item>
        <p>Salta para a região dinâmica anterior ou próxima</p>
      </item>
      <item>
        <p>Salta para a última região que apresentou informação</p>
      </item>
      <item>
        <p>Revê as últimas nove mensagens de regiões dinâmicas que foram apresentadas</p>
    </item>
    </list>
  </section>
</page>
