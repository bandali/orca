<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="commands_flat_review" xml:lang="da">
  <info>
    <link type="next" xref="commands_find"/>
    <link type="guide" xref="commands#reviewing_screen_contents"/>
    <link type="seealso" xref="howto_flat_review"/>
    <title type="sort">1. Flad gennemgang</title>
    <title type="link">Flad gennemgang</title>
    <desc>Kommandoer til rumlig gennemgang af vinduer</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Deling på samme vilkår 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2020-2022&lt;</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ask Hjorth Larsen</mal:name>
      <mal:email>asklarsen@gmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Kommandoer for flad gennemgang</title>
  <p>Orcas kommandoer for <link xref="howto_flat_review">flad gennemgang</link> er designet til rumlig gennemgang af elementer, som vises på skærmen. De gør det også muligt at læse den nuværende linje, ord og tegn ved navigering i dokumenttekst. De fleste af kommandoerne er “bundet” til tastetryk. Se venligst <link xref="howto_key_bindings">Ændring af tastebindinger</link> for information om, hvordan ubundne kommandoer bindes.</p>
  <section id="line">
    <title>Kommandoer til gennemgang en linje ad gangen</title>
    <list>
      <item>
        <p>Første linje (“begyndelsespositionen”):</p>
        <list>
          <item><p>Stationær: <keyseq><key>Orca-ændringstast</key><key>NT 7</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Ctrl</key><key>U</key></keyseq></p></item>
        </list>
      </item>
      <item>
        <p>Forrige linje:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 7</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>U</key></keyseq></p></item>
        </list>
      </item>
      <item>
        <p>Nuværende linje:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 8</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>I</key></keyseq></p></item>
        </list>
      </item>
        <item><p>Stav nuværende linje:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 8</key></keyseq> (dobbeltklikket)</p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>I</key></keyseq> (dobbeltklikket)</p></item>
        </list>
      </item>
      <item><p>Stav den nuværende linje fonetisk:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 8</key></keyseq> (tripelklikket)</p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>I</key></keyseq> (tripelklikket)</p></item>
        </list>
      </item>
      <item><p>Næste linje:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 9</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>O</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Sidste linje (“slutpositionen”):</p>
        <list>
          <item><p>Stationær: <keyseq><key>Orca-ændringstast</key><key>NT 9</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Ctrl</key><key>O</key></keyseq></p></item>
        </list>
      </item>
    </list>
  </section>
  <section id="word">
    <title>Kommandoer til gennemgang et ord ad gangen</title>
    <list>
      <item><p>Ord ovenfor:</p>
        <list>
          <item><p>Stationær: <keyseq><key>Orca-ændringstast</key><key>NT 4</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Ctrl</key><key>J</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Forrige ord:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 4</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>J</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Nuværende ord:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 5</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>K</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Stav nuværende ord:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 5</key></keyseq> (dobbeltklikket)</p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>K</key></keyseq> (dobbeltklikket)</p></item>
        </list>
      </item>
      <item><p>Stav nuværende ord fonetisk:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 5</key></keyseq> (tripelklikket)</p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>K</key></keyseq> (tripelklikket)</p></item>
        </list>
      </item>
      <item><p>Næste ord:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 6</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>L</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Ord nedenfor:</p>
        <list>
          <item><p>Stationær: <keyseq><key>Orca-ændringstast</key><key>NT 6</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Ctrl</key><key>L</key></keyseq></p></item>
        </list>
      </item>
    </list>
  </section>
  <section id="character">
    <title>Kommandoer til gennemgang et tegn ad gangen</title>
    <list>
      <item><p>Forrige tegn:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 1</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>M</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Nuværende tegn:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 2</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Komma</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Oplæs nuværende tegn fonetisk:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 2</key></keyseq> (dobbeltklikket)</p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Komma</key></keyseq> (dobbeltklikket)</p></item>
        </list>
      </item>
      <item><p>Oplæs unicodeværdien af nuværende tegn:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 2</key></keyseq> (tripelklikket)</p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Komma</key></keyseq> (tripelklikket)</p></item>
        </list>
      </item>
      <item><p>Næste tegn:</p>
        <list>
          <item><p>Stationær: <keyseq><key>NT 3</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Punktum</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Sidste tegn på nuværende linje:</p>
        <list>
          <item><p>Stationær: <keyseq><key>Orca-ændringstast</key><key>NT 1</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Ctrl</key><key>M</key></keyseq></p></item>
        </list>
      </item>
    </list>
  </section>
  <section id="other">
    <title>Yderligere kommandoer</title>
    <list>
      <item><p>Flad gennemgang til/fra (opdaterer flad gennemgang-konteksten):</p>
        <list>
          <item><p>Stationær: <keyseq><key>Orca-ændringstast</key><key>NT Minus</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>P</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Gennemgå nuværende punkt/kontrol:</p>
        <list>
          <item><p>Stationær: <keyseq><key>Orca-ændringstast</key><key>NT 5</key></keyseq></p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Ctrl</key><key>K</key></keyseq></p></item>
        </list>
      </item>
      <item><p>Brug Oplæs Alt til at gennemgå den nuværende dialog eller vindue:</p>
        <list>
          <item><p>Stationær: <key>NT Plus</key> (dobbeltklikket)</p></item>
          <item><p>Bærbar: <keyseq><key>Orca-ændringstast</key><key>Semikolon</key></keyseq> (dobbeltklikket)</p></item>
        </list>
      </item>
      <item>
      <p>Kopiér indholdet under flad gennemgang til udklipsholderen: (Ubundet)</p>
      </item>
        <item>
        <p>Tilføj indholdet under flad gennemgang til udklipsholderen: (Ubundet)</p>
      </item>
    </list>
  </section>
</page>
